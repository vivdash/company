class CreateJoinTableEmployeePolicy < ActiveRecord::Migration[5.2]
  def change
    create_join_table :employees, :policies do |t|
      t.index %i[employee_id policy_id]
      t.index %i[policy_id employee_id]
    end
  end
end
