FactoryBot.define do
  factory :employee do
    sequence(:name) { |n| "Name-#{n}" }
    email { Faker::Internet.email }
    phone { Faker::Number.number }
    company
  end
end
