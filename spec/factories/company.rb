FactoryBot.define do
  factory :company do
    sequence(:name) { |n| "All-#{n}" }
  end
end
