require 'rails_helper'

RSpec.describe EmployeesHelper, type: :helper do
  describe '#companies' do
    let(:companies) { FactoryBot.create_list(:company, 5) }

    it 'returns companies array' do
      expect(helper.companies).to eq(companies)
    end
  end
end
