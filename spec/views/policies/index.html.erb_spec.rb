require 'rails_helper'

RSpec.describe 'policies/index', type: :view do
  before(:each) do
    assign(:policies, [
      Policy.create!(
        name: 'Name',
        company: FactoryBot.create(:company)
      ),
      Policy.create!(
        name: 'Name1',
        company: FactoryBot.create(:company)
      )
    ])
  end

  it 'renders a list of policies' do
    render
    assert_select 'tr>td', text: /Name/, count: 2
  end
end
