require 'rails_helper'

RSpec.describe 'employees/index', type: :view do
  before(:each) do
    FactoryBot.create_list(:employee, 2)
    @employees = Employee.paginate(page: params[:page]).order('id DESC')
  end

  it 'renders a list of employees' do
    render
    assert_select 'tr>td', text: /Name/, count: 2
  end
end
