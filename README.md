# README

Steps to run app

rbenv local 2.5.1
bundle install
rake db:create db:migrate db:seed
rails s

Steps to run specs

rbenv local 2.5.1
bundle install
rake db:create db:migrate db:seed
bundle exec rspec
