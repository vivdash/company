class Company < ApplicationRecord
  include FileImporter

  has_many :employees
  has_many :policies

  validates :name, presence: true, uniqueness: true
  scope :ordered_by_name, -> { order(name: :asc) }

  def import_and_validate_emp(file)
    spreadsheet_path = validate_spreadsheet(file)
    get_valid_and_invalid_emp(spreadsheet_path)
  end

  private

  def get_valid_and_invalid_emp(spreadsheet_path)
    valid_employees = []
    invalid_employees = []
    CSV.foreach(spreadsheet_path, headers: true) do |row|
      employee = init_employee(row)
      (valid_employees << employee ; next) if employee.valid?
      invalid_employees << employee
    end
    Employee.import(valid_employees, validate: false, batch_size: 100)
    {
      valid_emp_count: valid_employees.count,
      invalid_employees: invalid_employees
    }
  end

  def init_employee(row)
    employee = employees.new(name: row['Employee Name'],
                             email: row['Email'],
                             phone: row['Phone'],
                             parent_id: Employee.find_by_email(row['Report To'])&.id
    )
    employee.policy_ids = get_policies(row['Assigned Policies'].split('|'))
    employee
  end

  def get_policies(policy_names)
    exsisting_policies = policies.where(name: policy_names)
    exsisting_policies_ids = exsisting_policies.pluck(:id)
    (policy_names - exsisting_policies.pluck(:name)).each do |policy|
      exsisting_policies_ids << policies.create(name: policy).id
    end
    exsisting_policies_ids
  end
end
