require 'csv'

class Employee < ApplicationRecord
  belongs_to :company
  has_and_belongs_to_many :policies
  self.per_page = 10
  scope :latest, -> { order(id: :desc) }

  validates :name, presence: true
  validates :email, uniqueness: true, presence: true

  acts_as_nested_set
end
