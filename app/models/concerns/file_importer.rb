require 'csv'

module FileImporter
  extend ActiveSupport::Concern

  def validate_spreadsheet(file)
    case File.extname(file.original_filename)
    when '.csv' then file.path
    else raise "Unknown file type: #{file.original_filename}"
    end
  end
end
