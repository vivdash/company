class ImportEmployeesNotificationService
  def call(valid_count_and_invalid_emp)
    {
      success: get_success(valid_count_and_invalid_emp[:valid_emp_count]),
      failure: get_failure(valid_count_and_invalid_emp[:invalid_employees])
    }
  end

  private

  def get_failure(invalid_employees)
    return if invalid_employees.blank?
    failure_message = "Errors in Adding Employees Count: #{invalid_employees.count} <br/>"
    invalid_employees.each do |employee|
      employee.errors.full_messages.each do |message|
        failure_message << "#{employee.email || employee.name}: #{message} <br/>"
      end
    end
    failure_message
  end

  def get_success(valid_emp_count)
    "Successfully Added Employees Count: #{valid_emp_count}"
  end
end
