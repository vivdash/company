Rails.application.routes.draw do
  resources :policies
  resources :companies
  resources :employees do
    collection do
      get 'bulk_import'
      post 'bulk_import'
    end
  end
  root to: 'employees#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
